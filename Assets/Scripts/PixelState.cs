using System;

[Serializable]
public class PixelState
{
    public int x;
    public int y;
    public bool filled;
}
