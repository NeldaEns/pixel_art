using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    public static DataManager ins;

    private List<PixelState> pixelStates = new List<PixelState>();

    private const string pixel_state = "pixel_state";

    private void Awake()
    {
        if(ins == null)
        {
            ins = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void SavePixelStates()
    {
        pixelStates.Clear();

        Pixel[] pixels = FindObjectsOfType<Pixel>();
        foreach(var pixel in pixels)
        {
            pixelStates.Add(new PixelState
            {
                x = (int)pixel.transform.position.x,
                y = (int)pixel.transform.position.y,
                filled = pixel.IsFilledIn()
            });
        }
        string json = JsonHelper.ToJson(pixelStates);
        PlayerPrefs.SetString(pixel_state, json);
        PlayerPrefs.Save();
    }

    public void LoadPixelStates()
    {
        string json = PlayerPrefs.GetString(pixel_state);
        pixelStates = JsonHelper.FromJson<PixelState>(json);
        foreach(var pixelState in pixelStates)
        {
            Pixel pixel = FindPixelAtPosition(pixelState.x, pixelState.y);
            if(pixel != null)
            {
                if(pixelState.filled)
                {
                    pixel.Fill();
                }
                else
                {
                    pixel.FillWrong();
                }
            }
        }
    }

    private Pixel FindPixelAtPosition(int x, int y)
    {
        Pixel[] pixels = FindObjectsOfType<Pixel>();
        foreach (var pixel in pixels)
        {
            if ((int)pixel.transform.position.x == x && (int)pixel.transform.position.y == y)
            {
                return pixel;
            }
        }
        return null;
    }
}
