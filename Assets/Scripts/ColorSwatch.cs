using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ColorSwatch : MonoBehaviour
{
    public int colorID;

    private bool colorCompleted;
    private bool colorSelected;
    public TextMeshPro colorTxt;
    public SpriteRenderer backGround;
    public SpriteRenderer border;

    public void SetData(int id, Color color)
    {
        colorID = id;
        colorTxt.text = id.ToString();
        backGround.color = color;
    }

    public void SetCompleted()
    {
        colorCompleted = true;
        colorTxt.text = "";
    }

    public void SetSelected(bool selected)
    {
        if(!colorCompleted)
        {
            colorSelected = selected;
            if(colorSelected)
            {
                border.color = Color.gray;
            }
            else
            {
                border.color = Color.black;
            }
        }
    }

}
