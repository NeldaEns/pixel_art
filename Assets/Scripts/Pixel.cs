using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Pixel : MonoBehaviour
{
    public int colorID;

    public TextMeshPro colorTxt;
    private Color pixelColor;

    public SpriteRenderer backGround;
    public SpriteRenderer border;

    public void SetData(Color color, int colorID1)
    {
        colorID = colorID1;
        pixelColor = color;
        border.color = new Color(0.95f, 0.95f, 0.95f, 1);
        colorTxt.text = colorID1.ToString();

        backGround.color = Color.Lerp(new Color(pixelColor.grayscale, pixelColor.grayscale, pixelColor.grayscale), Color.white, 0.85f);
    }

    public bool IsFilledIn()
    {
        if(backGround.color == pixelColor)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void SetSelected(bool selected)
    {
        if(selected)
        {
            if(!IsFilledIn())
            {
                backGround.color = new Color(0.5f, 0.5f, 0.5f, 1);
            }
        }
        else
        {
            if (!IsFilledIn())
            {
                backGround.color = Color.Lerp(new Color(pixelColor.grayscale, pixelColor.grayscale, pixelColor.grayscale), Color.white, 0.85f);
            }
        }
    }

    public void Fill()
    {
        if(!IsFilledIn())
        {
            border.color = pixelColor;
            backGround.color = pixelColor;
            colorTxt.text = "";
        }
    }

    public void FillWrong()
    {
        if(!IsFilledIn())
        {
            backGround.color = new Color(1, 170 / 255f, 170 / 255f, 1);
        }
    }

}
