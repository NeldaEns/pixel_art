﻿using UnityEngine;


public class Camera2D : MonoBehaviour
{
    public Transform area;

    private float top;
    private Vector2 size;
    private Vector3 offset;
    private float pixelUnits;
    private Vector3 velocity = Vector3.zero;

    private Vector3 dragOrigin;

    private float distance = 35f ;
    private bool lockZoom;
    public float zoomVelocity;
    private float zoomSpeed = 50.0f;
    private float zoomMin = 9.6f, zoomMax = 35f;

    private float currentDistance;

    Camera cameraView;

    private void Awake()
    {
        cameraView = transform.gameObject.GetComponent<Camera>();
    }


    private void Start()
    { 
        Set();
    }

    public void FixedUpdate()
    {
        cameraView.orthographicSize = distance;
    }

    public float Getdistance()
    {
        return distance;

    }
    public void Setdistance(float distance1)
    {
        distance = distance1;
        cameraView.orthographicSize = distance;
    }

    public void Update()
    {
        if (!lockZoom)
        {
            float scroll = Input.GetAxis("Mouse ScrollWheel");
            if (scroll < 0 && zoomVelocity > 0 || scroll > 0 && zoomVelocity < 0)
                zoomVelocity = 0;

            zoomVelocity += Time.deltaTime * scroll * zoomSpeed;
            distance -= zoomVelocity;
            distance = Mathf.Clamp(distance, zoomMin, zoomMax);

            zoomVelocity = Mathf.Lerp(zoomVelocity, 0, 0.1f);
        }
        //if (!lockZoom)
        //{
        //    if (Input.touchCount == 2)
        //    {
        //        Touch touch1 = Input.GetTouch(0);
        //        Touch touch2 = Input.GetTouch(1);

        //        Vector2 touch1PrevPos = touch1.position - touch1.deltaPosition;
        //        Vector2 touch2PrevPos = touch2.position - touch2.deltaPosition;
        //        float prevDistance = (touch1PrevPos - touch2PrevPos).magnitude;
        //        float currentDistance = (touch1.position - touch2.position).magnitude;

        //        float zoomSpeed = 0.1f;
        //        float deltaDistance = prevDistance - currentDistance;
        //        distance += deltaDistance * zoomSpeed;
        //        distance = Mathf.Clamp(distance, zoomMin, zoomMax);
        //    }
        //}
        PanCamera();
    }

    public void PanCamera()
    {
        if(Input.GetMouseButtonDown(0))
        {
            dragOrigin = cameraView.ScreenToWorldPoint(Input.mousePosition);
        }

        if(Input.GetMouseButton(0))
        {
            Vector3 difference = dragOrigin - cameraView.ScreenToWorldPoint(Input.mousePosition);

            cameraView.transform.position += difference;

        }
    }

    public void Set()
    {
        if (cameraView == null)
            cameraView = transform.gameObject.GetComponent<Camera>();

        float height = area.localScale.y * 100;
        float width = area.localScale.x * 100;

        float w = Screen.width / width;
        float h = Screen.height / height;

        float ratio = h / w;
        float size = (height / 2) / 100f;

        if (h < w)
            size /= ratio;

        cameraView.orthographicSize = size;

        Vector2 position = area.transform.position;

        //Vector3 camPosition = position;
        //Vector3 point = cameraView.WorldToViewportPoint(camPosition);
        //Vector3 delta = camPosition - cameraView.ViewportToWorldPoint(new Vector3(-0.25f, 0.75f, point.z));
        //Vector3 destination = transform.position + delta;
        //transform.position = destination;
        //RefreshBounds();
        area.transform.position = cameraView.transform.position;
    }

    private void RefreshBounds()
    {
        Sprite sprite = area.gameObject.GetComponent<SpriteRenderer>().sprite;
        pixelUnits = sprite.rect.width / sprite.bounds.size.x;

        size = new Vector2(area.transform.localScale.x * sprite.texture.width / pixelUnits,
            area.transform.localScale.y * sprite.texture.height / pixelUnits);

        offset = area.transform.position;

        var vertExtent = cameraView.orthographicSize;

        top = size.y / 2.0f - vertExtent + offset.y;

        Vector3 v3 = transform.position;
        v3.y = top;
        transform.position = v3;
    }

}
