using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager ins;

    [Header("Library")]
    public GameObject libraryObj;
    public GameObject libraryObjOn;
    public GameObject libraryObjOff;
    public GameObject eventObj;
    public GameObject newObj;
    public GameObject bonusObj;
    public GameObject bookObj;

    [Header("Daily")]
    public GameObject dailyObj;
    public GameObject dailyObjOn;
    public GameObject daylyObjOff;

    [Header("MyWorks")]
    public GameObject myWorkObj;
    public GameObject myWorkOn;
    public GameObject myWorkOff;
    public GameObject myArt;
    public GameObject doingArt;

    [Header("Create")]
    public GameObject createObj;
    public GameObject createObjOn;
    public GameObject createObjOff;

    private void Awake()
    {
        ins = this;
    }


    public void OpenLibrary()
    {
        libraryObj.SetActive(true);
        dailyObj.SetActive(false);
        myWorkObj.SetActive(false);
        createObj.SetActive(false);
        libraryObjOn.SetActive(true);
        libraryObjOff.SetActive(false);
        dailyObjOn.SetActive(false);
        daylyObjOff.SetActive(true);
        myWorkOn.SetActive(false);
        myWorkOff.SetActive(true);
        createObjOn.SetActive(false);
        createObjOff.SetActive(true);
    }

    public void OpenDaily()
    {
        libraryObj.SetActive(false);
        dailyObj.SetActive(true);
        myWorkObj.SetActive(false);
        createObj.SetActive(false);
        libraryObjOn.SetActive(false);
        libraryObjOff.SetActive(true);
        dailyObjOn.SetActive(true);
        daylyObjOff.SetActive(false);
        myWorkOn.SetActive(false);
        myWorkOff.SetActive(true);
        createObjOn.SetActive(false);
        createObjOff.SetActive(true);
    }

    public void OpenMyWork()
    {
        libraryObj.SetActive(false);
        dailyObj.SetActive(false);
        myWorkObj.SetActive(true);
        createObj.SetActive(false);
        libraryObjOn.SetActive(false);
        libraryObjOff.SetActive(true);
        dailyObjOn.SetActive(false);
        daylyObjOff.SetActive(true);
        myWorkOn.SetActive(true);
        myWorkOff.SetActive(false);
        createObjOn.SetActive(false);
        createObjOff.SetActive(true);
    }

    public void OpenCreate()
    {
        libraryObj.SetActive(false);
        dailyObj.SetActive(false);
        myWorkObj.SetActive(false);
        createObj.SetActive(true);
        libraryObjOn.SetActive(false);
        libraryObjOff.SetActive(true);
        dailyObjOn.SetActive(false);
        daylyObjOff.SetActive(true);
        myWorkOn.SetActive(false);
        myWorkOff.SetActive(true);
        createObjOn.SetActive(true);
        createObjOff.SetActive(false);
    }

}
