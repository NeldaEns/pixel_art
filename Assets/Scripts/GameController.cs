﻿using UnityEngine;
using System.Collections.Generic;

public class GameController : MonoBehaviour
{

    public static GameController ins;
    public Texture2D textureArt;
    Pixel[,] pixels;
    Camera cameraView;
    Camera2D camPos;

    int ID = 1;
    Dictionary<Color, int> colorsw = new Dictionary<Color, int>();
    List<ColorSwatch> colorSwatches = new List<ColorSwatch>();

    Dictionary<int, List<Pixel>> pixelGroups = new Dictionary<int, List<Pixel>>();

    RaycastHit2D[] hits = new RaycastHit2D[1];
    ColorSwatch selectedColorSwatch;
    public GameObject pixelPrefab;
    public GameObject colorSwatchPrefab;
    public GameObject pixelParent;
    public GameObject colorParent;

    void Awake()
    {
        ins = this;
    }

    private void Start()
    {
        cameraView = Camera.main;

        CreatePixelMap();
        CreateColorSwatches();
    }

    void CreatePixelMap()
    {
        Color[] colors = textureArt.GetPixels();
        pixels = new Pixel[textureArt.width, textureArt.height];

        for (int x = 0; x < textureArt.width; x++)
        {
            for (int y = 0; y < textureArt.height; y++)
            {
                if (colors[x + y * textureArt.width].a != 0)
                {
                    GameObject pixel = Instantiate(pixelPrefab, new Vector3(0, 0, 0),Quaternion.identity);
                    pixel.transform.SetParent(pixelParent.transform);
                    pixel.transform.position = new Vector3(x, y);
                    int id = ID;
                    if (colorsw.ContainsKey(colors[x + y * textureArt.width]))
                    {
                        id = colorsw[colors[x + y * textureArt.width]];
                    }
                    else
                    {
                        colorsw.Add(colors[x + y * textureArt.width], ID);
                        ID++;
                    }

                    pixels[x, y] = pixel.GetComponent<Pixel>();
                    pixels[x, y].SetData(colors[x + y * textureArt.width], id);

                    if (!pixelGroups.ContainsKey(id))
                    {
                        pixelGroups.Add(id, new List<Pixel>());
                    }
                    pixelGroups[id].Add(pixels[x, y]);
                }
            }
        }
    }

    void CreateColorSwatches()
    {
        foreach (KeyValuePair<Color, int> kvp in colorsw)
        {
            GameObject colorSwatch = Instantiate(colorSwatchPrefab, new Vector3(5,5,0), Quaternion.identity);
            colorSwatch.transform.SetParent(colorParent.transform);
            //float offset = 2f;
            //colorSwatch.transform.position = new Vector3(kvp.Value * 2 * offset, -10, 1);
            ColorSwatch colorSwatch1 = colorSwatch.GetComponent<ColorSwatch>();
            colorSwatch1.SetData(kvp.Value, kvp.Key);
            colorSwatches.Add(colorSwatch1);
        }
    }

    void DeselectAllColorSwatches()
    {
        for (int n = 0; n < colorSwatches.Count; n++)
        {
            colorSwatches[n].SetSelected(false);
        }
    }

    void Update()
    {
        Vector2 mousePos = cameraView.ScreenToWorldPoint(Input.mousePosition);
        int x = Mathf.FloorToInt(mousePos.x);
        int y = Mathf.FloorToInt(mousePos.y);

        Pixel hoveredPixel = null;
        if (x >= 0 && x < pixels.GetLength(0) && y >= 0 && y < pixels.GetLength(1))
        {
            if (pixels[x, y] != null)
            {
                hoveredPixel = pixels[x, y];
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            int hitCount = Physics2D.RaycastNonAlloc(mousePos, Vector2.zero, hits);
            for (int n = 0; n < hitCount; n++)
            {
                if (hits[n].collider.CompareTag("ColorSwatch"))
                {
                    SelectColorSwatch(hits[n].collider.GetComponent<ColorSwatch>());
                }
            }
        }

        if (Input.GetMouseButton(0))
        {
            if (hoveredPixel != null && !hoveredPixel.IsFilledIn())
            {
                if (selectedColorSwatch != null && selectedColorSwatch.colorID == hoveredPixel.colorID)
                {
                    hoveredPixel.Fill();
                    if (CheckIfSelectedComplete())
                    {
                        selectedColorSwatch.SetCompleted();
                    }
                }
                else
                {
                    hoveredPixel.FillWrong();
                }
            } 
            
        }
    }

    void SelectColorSwatch(ColorSwatch swatch)
    {
        if (selectedColorSwatch != null)
        {
            for (int n = 0; n < pixelGroups[selectedColorSwatch.colorID].Count; n++)
            {
                pixelGroups[selectedColorSwatch.colorID][n].SetSelected(false);
            }

            selectedColorSwatch.SetSelected(false);
        }

        selectedColorSwatch = swatch;
        selectedColorSwatch.SetSelected(true);

        for (int n = 0; n < pixelGroups[selectedColorSwatch.colorID].Count; n++)
        {
            pixelGroups[selectedColorSwatch.colorID][n].SetSelected(true);
        }
    }

    bool CheckIfSelectedComplete()
    {
        if (selectedColorSwatch != null)
        {
            for (int n = 0; n < pixelGroups[selectedColorSwatch.colorID].Count; n++)
            {
                if (pixelGroups[selectedColorSwatch.colorID][n].IsFilledIn() == false)
                    return false;
            }
        }
        return true;
    }

}
